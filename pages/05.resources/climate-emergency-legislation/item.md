---
title: Climate Emergency Legislation
date: 00:00 08/08/2019
taxonomy:
  category: Legislation
  type: PDF
  tag: [Climate Emergency Declaration, Legislation, New York]
published: false
---

Resources meant to help groups get our governments to tell the truth by declaring a **Climate Emergency**.

===

## External Resources

[The Climate Mobilization](https://www.theclimatemobilization.org/climate-emergency-resolution) has resources and templates to drive **Climate Emergency** legislation.  
[CACE](https://www.caceonline.org/) has great resources on building campaings to get your local councils to declare a **Climate Emergency**.

## Examples of Cities Declaring a Climate Emergency

### Austin, Texas Declares A Climate Emergency 08/08/2019

<a href="/user/pages/05.resources/climate-emergency-legislation/austin-res-no.pdf">Draft Resolution <i class="fas fa-file-pdf"></i></a>

### Boulder, Colorado Declares A Climate Emergency 08/01/2019

<a href="/user/pages/05.resources/climate-emergency-legislation/boulder-res-no-1260.pdf">Legislation PDF <i class="fas fa-file-pdf"></i></a>

#### Videos

[plugin:youtube](https://www.youtube.com/watch?v=cjjpyeVhyDQ)
/#### Articles
[City of Boulder](https://bouldercolorado.gov/newsroom/city-council-declares-climate-emergency)

### Ojai, California Declares A Climate Emergency 07/22/2019

<a href="/user/pages/05.resources/climate-emergency-legislation/ojai-res-no-19-30.pdf">Legislation PDF <i class="fas fa-file-pdf"></i></a>

### New York, New York Declares A Climate Emergency 06/26/2019

<a href="/user/pages/05.resources/climate-emergency-legislation/ny-res-no-864.pdf">Legislation PDF <i class="fas fa-file-pdf"></i></a>

#### Videos

[plugin:youtube](https://www.youtube.com/watch?v=XPtAKjhT02s)

### Oakland, California Declares A Climate Emergency 10/30/2018

<a href="/user/pages/05.resources/climate-emergency-legislation/oakland-res-no-87397.pdf">Legislation PDF <i class="fas fa-file-pdf"></i></a>
