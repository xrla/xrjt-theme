---
title: "Principles"
classes: "text-left bg-blue"
---

# Principles{.red-line}

Like the international XR group we have ten basic principals from which our actions grow.

**They are:**

1. All of us in XRCAL have a shared vision of change.
1. XRCAL's mission is what is necessary — mobilizing 3.5% or more of California's population to achieve systemic change in California in the service of California's part in maintaining a 1.5oC increase of average global temperature over preindustrial averages.
1. XRCAL believes that California and Californians need a sustainable culture. Our culture must be healthy for all, resilient, and adaptable to face a future destabilized by our society's previously disastrous choices.
1. XRCAL openly challenges its members to rise up to remake the toxic systems of governance and economics that we have created in the U.S. XRCAL believes that _everyone_ in the U.S. must question their role in perpetuating an ideology that threatens the very survival of our planet as we recall it in our recent past and know it today and — even and especially XRCAL members!
1. XRCAL values reflecting on and learning from our group's public actions and those of other popular movements. We will take the lessons we learn to plan for more action with each public action learning
   from and building on the failures and successes of the previous public actions.
1. XRCAL welcomes everyone and every part of everyone willing to work within our framework. We work to actively create safer and more accessible spaces open to all who come with a genuine purpose to help us achieve our goals in California.
1. XRCAL actively seeks to recast power dynamics and structures in California with the goal of remaking existing local power hierarchies to foster more equitable and just participation by all Californians.
1. XRCAL will not blame nor shame. We believe our present system of governance and economics to be toxic and ultimately deadly for the earth as we know it to be. We acknowledge that we all have and continue to play parts in creating and perpetuating this system. No one is without blame and no one is unable to work to make the changes required for the healthy survival of all. We are in this together, we have created this crisis, we must work to avert this crisis.
1. XRCAL is a non-violent cell in a non-violent network that gains its strength as a part of a whole. We will exclusively use non-violent strategies and tactics to work to achieve our goals and believe them to be the most effective way to foster the change we require.
1. XRCAL exists as an autonomous part of a decentralized whole and our individual group practices collective decision making and has no leadership which might be pointed to as "in charge" of any particular decision or action. We are a collective creating a collective structure with which we work to challenge a deadly status quo. We as individuals in XRCAL act in accordance with the objective moral principal that exploiting our planet's resources in a manner that creates the likely probability of the destruction of life on earth as we understand it today is categorically wrong _and must be stopped_.

<div class="text-center"><img src="/user/themes/xrla/images/xr-hourglass-black.svg" width="20"/> **Any who are able to follow these ten core principals are welcome to join our collective action.** <img src="/user/themes/xrla/images/xr-hourglass-black.svg" width="20"/></div>

- We refuse to bequeath a dying planet to future generations by failing to act now.
- We act in peace with ferocious love of these lands in our hearts.
