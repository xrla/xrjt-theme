---
title: "04"
---

# Just Transition{.red-line .inline-block #just-transition}

XRCAL requires a just transition that prioritizes California's most vulnerable people, indigenous sovereignty, and establishes reparations and remediation, led by and for, California's African-Americans, indigenous people, people of color, and poor communities as recompense for their suffering years of structural environmental injustice. XRCAL understands that establishing legal rights for ecosystems is a non-negotiable requirement of California's governments in order for historically degraded ecosystems to thrive and regenerate in perpetuity. The repair of the effects of historical and continuing ecocide in California is absolutely mandatory to prevent the extinction of our human, and all of earth's, species.
