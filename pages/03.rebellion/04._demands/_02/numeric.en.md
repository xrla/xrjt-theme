---
title: "02"
class: "underline yellow-line"
---

# Net Zero By 2025{.red-line .inline-block #net-zero}

That California governments must enact legally-binding policy measures to reduce
carbon emissions to net zero by 2025<sup><a href="#footnote3">2</a></sup> and to reduce consumer consumption levels and impacts within 50 State, County, and Municipal governments and among all of the citizens of California.
