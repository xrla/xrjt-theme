---
title: 'FAQS'
published: false
---
# FAQS

## Aren’t you just a group of middle-class left-wing activists?

Extinction Rebellion is made up of people of all ages and backgrounds from all over the world. From under 18 to over 80 year olds – there are thousands of people willing to put their liberty on the line to fight the climate and ecological emergency and protect biodiversity and atmospheric health.

## We don’t align with any political party and welcome people who vote for all political parties and none.

We are working to improve diversity in our movement. We don’t think it is helpful to set this up as a fight between the “left” and the “right”, we have an enormous challenge before us, we believe we need to lay down our differences and find our common ground.

## Who is behind Extinction Rebellion?

Extinction Rebellion is a movement made up of people from all walks of life. It started in response to the IPCC report that we only have 12 years to stop catastrophic climate change and our understanding that we have entered the 6th mass extinction event. It started small and was launched in the UK on October 31st 2018, it has grown very quickly and there are now about 130 Extinction Rebellion groups across the UK from Cornwall to Sunderland to Glasgow and there are groups across the world. The majority of people who work for Extinction Rebellion are volunteers, a small percentage is receiving living expenses. Decisions are made based on a model of organising called a “self organising system” – people do what they can according to the skills and time they can offer.

## Where did the idea come from?

Extinction Rebellion has come about as a result of academic research and mass engagement. Roger Hallam and Dr Gail Bradbrook …..did some thinking and talking…..and following discussions with others a movement was born. The focus since then has been on speaking to people either online or in town halls. Fellow founders such as Robin Boardman has visited towns across the UK and the response was huge.  