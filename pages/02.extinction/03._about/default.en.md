---
title: 'About Us'
class: 'bg-lightgrey'
published: false
---

# About Us

Extinction Rebellion is an international movement that uses non-violent civil disobedience in an attempt to halt mass extinction and mimimise the risk of social collapse.
