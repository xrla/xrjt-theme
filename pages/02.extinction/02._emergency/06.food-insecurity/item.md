---
title: 124 million
subtitle: "People suffering acute food insecurity"
external_link: 
--- 

More frequent and severe water extremes, including droughts and floods, impact agricultural production, while rising temperatures translate into increased water demand in agriculture sectors.       

“We have already observed impacts of climate change on agriculture. We have assessed the amount of climate change we can adapt to. There’s a lot we can’t adapt to even at 2ºC. At 4ºC the impacts are very high and we cannot adapt to them” – Dr. Rachel Warren, University of East Anglia