---
title: 2x
subtitle: "Water withdrawals grew at almost twice the rate of population increase"
external_link: 
--- 

Water withdrawals grew at almost twice the rate of population increase in the twentieth century.

The global water cycle is intensifying due to climate change, with wetter regions generally becoming wetter and drier regions becoming even drier. A 2018 UN report highlights that at present, an estimated 3.6 billion people (nearly half the global population) live in areas that are potentially water-scarce at least one month per year, and this population could increase to some 4.8–5.7 billion by 2050.