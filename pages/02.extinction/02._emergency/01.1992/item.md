---
title: 1992
subtitle: “World Scientists’ Warning to Humanity”
external_link: https://www.ucsusa.org/about/1992-world-scientists.html
--- 

In 1992, the Union of Concerned Scientists including the majority of living science Nobel laureates, penned the [“World Scientists’ Warning to Humanity”](https://www.ucsusa.org/about/1992-world-scientists.html) calling on humankind to curtail environmental destruction and warning that “a great change in our stewardship of the Earth and the life on it is required, if vast human misery is to be avoided.” They showed that humans were on a collision course with the natural world. They proclaimed that fundamental changes were urgently needed to avoid the consequences our present course would bring.

The authors of the 1992 declaration feared that humanity was pushing Earth’s ecosystems beyond their capacities to support the web of life. They described how we are fast approaching many of the limits of what the biosphere can tolerate without substantial and irreversible harm. They implored that we cut greenhouse gas (GHG) emissions and phase out fossil fuels, reduce deforestation, and reverse the trend of collapsing biodiversity.
