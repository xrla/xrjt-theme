---
title: 30%
subtitle: "The oceans have already become 30% more acidic"
external_link: 
internal_link:  
--- 

The oceans are already become 30% more acidic, as carbon dioxide from the burning of fossil fuels dissolves it alters the chemistry of the sea water. On our current emission trajectory, in 2100, the pH increase of the ocean will see a 150% increase in acidity! This will affect marine life from shellfish to whole coral reef communities by removing needed minerals that they use to grow their shells. The oceanic conditions will be unlike marine ecosystems have experienced for the last 14 million years.

![Ocean PH Projections](ocean-ph-projections.gif)  
