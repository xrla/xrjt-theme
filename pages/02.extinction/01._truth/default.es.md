---
title: 'La Verdad'
---
# La Verdad{.purple-line}

Nos enfrentamos a una emergencia global sin precedentes. La vida en la Tierra está en crisis: los científicos están de acuerdo en que hemos entrado en un período de abrupto colapso climático y estamos en medio de una extinción masiva de nuestra propia creación.
