---
title: Footer
routable: false
visible: false
left: 
    # -
    #     title: La Rebelion

    #     links: 
    #         - 
    #             name: About
    #             url: /rebellion#about
    #         -
    #             name: Los Objetivos
    #             url: /rebellion#demands
    #         - 
    #             name: Principios
    #             url: /rebellion#principles
    # - 
    #     title: La Extinción
    #     links:
    #         - 
    #             name: La Verdad
    #             url: /extinction#the_truth
    #         - 
    #             name: The Extinction
    #             url: /extinction#emergency
middle:
    # -
    #     title: Other
    #     links: 
    #         - 
    #             name: Allies
    #             url: /allies
    #         - 
    #             name: News
    #             url: /news
    #         -
    #             name: Contact
    #             url: /#take_action
---