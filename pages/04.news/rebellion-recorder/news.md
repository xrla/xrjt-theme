---
title: Rebellion Recorder Issues
hide_git_sync_repo_link: false
show_sidebar: false
show_breadcrumbs: true
show_pagination: true
content:
    items: '@self.children'
    limit: 12
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
submenu:
    # articles:
    #     title: Articles
    #     tag: Article
    recorder:
        title: Rebellion Recorder
        uri: news/rebellion-recorder
bricklayer_layout: true
display_post_summary:
    enabled: false
pagination: true
hero_image: masthead.jpg
body_classes: hero-contain overlay-light hero-narrow
---
