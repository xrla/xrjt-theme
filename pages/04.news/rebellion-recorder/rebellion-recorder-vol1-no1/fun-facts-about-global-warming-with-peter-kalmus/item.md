---
title: Fun Facts About Global Warming With Peter Kalmus
subtitle: Physicist At NASA’s Jet Propulsion Lab And Author
author: Peter Kalmus
date: 00:00 04/15/2019
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
    category: Rebellion Recorder
    author: Peter Kalmus
    tag:
        - NASA
        - Climate Science
---

*Peter Kalmus —Climate Scientist*  

<i class="fas fa-skull-crossbones"></i> The current extinction rate is 1000 times above the normal background rate.  

<i class="fas fa-skull-crossbones"></i> By 2050, climate breakdown alone is projected to commit 15-37% of all species to extinction.  

<i class="fas fa-skull-crossbones"></i> 60% of Earth’s wild mammals, fish, birds, and reptiles have been wiped out since 1970.  

<i class="fas fa-skull-crossbones"></i> Insects are declining precipitously around the world.  

<i class="far fa-clock"></i> It takes 10 million years for biodiversity to recover after a mass extinction event. The decisions we make (or don’t make) in the next few years could have dire implications for the next millions of years.  

<i class="fas fa-bomb"></i> Much of the CO2 we emit today will remain in the atmosphere for thousands of years.
N At 1.5&deg;C of mean global warming, 70%-90% of corals are projected to die. At 2&deg;C of warming, 99% of corals are projected to die.  

<i class="fas fa-skull-crossbones"></i> The last 5 years were also the 5 hottest years ever recorded.  

<i class="fab fa-hotjar"></i> The last 18 years were 18 of the 19 hottest years on record.  

<i class="fab fa-hotjar"></i> Global CO2 emissions are increasing exponentially, as is atmospheric CO2 fraction, at over 2% per year.  

<i class="fas fa-chart-line"></i> In 2018 humans emitted 2.7% more CO2 than in 2017.  

<i class="fas fa-bomb"></i> Permafrost and wetland CO2 and methane emissions are not included in future projections of warming.  

<i class="fas fa-bomb"></i> Current CO2 and temperature increases have no analog within at least the last 50 million years.  

<i class="fas fa-skull-crossbones"></i> Heatwaves will become significantly more intense, frequent, and long-lasting. At 2&deg;C of warming, heatwaves over land will be 4&deg;C (7&deg;F) hotter on average, and one in 20 year events (in today’s climate) will occur 340% more frequently. This trend will continue as long as we keep burning fossil fuel.  

<i class="fas fa-skull-crossbones"></i> The 2003 European heatwave (>70,000 heat-related deaths) was made twice as likely by climate breakdown.  

<i class="fab fa-hotjar"></i> By 2015 extreme heatwaves had become ten times more likely than they were in 2004.  

<i class="fas fa-bomb"></i> An estimated 0.5&deg;C of additional warming is “baked in” even if we quit fossil fuel cold turkey today.  

<i class="fas fa-sun"></i> The Arctic will likely be nearly ice-free in summer by the 2040s. Historically, Arctic sea ice projections have consistently underestimated reality of sea ice loss.  

<i class="fab fa-hotjar"></i> Glaciers and snowpack are melting away around the world. They are a critical source of fresh water for drinking and agriculture for billions of people.  

<i class="fab fa-hotjar"></i> Sea level rise is accelerating, and could result in 2 billion climate refugees by 2100. Historically, sea level rise projections have consistently been revised upward.  

<i class="fab fa-hotjar"></i> Heavy rain events are increasing in intensity and frequency globally. Frequency of extreme rainfall over land is projected to increase by 17% at 1.5&deg;C of warming and by 36% at 2&deg;C of warming.  

<i class="far fa-hand-point-down"></i> Barley and wheat are expected to contain 15% and 8% less protein respectively by 2050.  

<i class="fab fa-hotjar"></i> By 2100, large swaths of the tropics, including Indonesia, Southern India, central Africa, the Amazon basin, and central America are projected to become uninhabitable due to deadly heat.  

<i class="fas fa-cut"></i> For a 66% chance to stay under 1.5&deg;C of warming, the IPCC SR1.5 estimates we need to cut global greenhouse gas emissions in half by 2030 and to zero by 2055. However, many climate scientists feel these estimates are grossly optimistic.  

<i class="fas fa-heart-broken"></i> Global poverty is projected to increase with warming, and several hundred million more people will be impoverished by 2050 on a 2&deg;C warming track than on on a 1.5&deg;C track.  

So, so much more, but I’m going to bed now.

*— Peter Kalmus, author of 'Being the Change, Live Well and Spark a Climate Revolution,' researches ways to make specific ecosystems forcasts more accurate. [peterkalmus.net](https://peterkalmus.net/)*