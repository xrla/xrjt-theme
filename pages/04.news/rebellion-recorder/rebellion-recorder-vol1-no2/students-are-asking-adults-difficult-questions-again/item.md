---
title: Students Are Asking Adults Difficult Questions, Again
subtitle: How Can Their Futures Be Sold Out So Easily?
author: Jesus Villalba
date: "00:00 10/07/2019"
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
  category: Rebellion Recorder
  author: Jesus Villalba
  tag:
    - Youth
    - Student Activists
hero_image: student-climate-strike.jpg
body_classes: overlay-pink
hero_caption: "Student climate strikes have been a powerful influence on public perceptions of the climate crisis worldwide."
header_image_credit: J. Matt
---

_Jesus Villalba — Student Organizer_

On September 20, 2019, in Los Angeles students from across the county gathered in Pershing Square for LA’s third ever Global Climate Strike. From noon to 3:30 p.m. students demonstrated, listened to speeches from frontline community members, enjoyed the performances of local artists, and marched on downtown LA.

Climate justice has been the goal of the strikes, with demands made by Los Angeles Youth that include the implementation of a National Green New Deal, the recognition of Indigenous and First Nations lands, immigration reform, and the recognition of climate refugees — as well as a moratorium on all carbon infrastructure, which includes a just transition for blue-collar workers.

While there are many individual and often related demands linked to the student strikes in Los Angeles, the main goal is clear: the youth want adults to pay attention and to act. After knowing about climate change for more than 40 years, current and past administrations have done little to nothing about the issue.

As we roll closer to a darker future, the issue has stopped being climate change and become a climate crisis. Adults have kicked the can too far down the road, and it is not fair to future generations to leave this unfinished business lying around. To do so would be to condemn humanity, us, to death.
Youth Climate Strike Los Angeles (YCSLA) debuted at LA’s first Global Climate Strike last March 15. Locally, a thousand students participated by marching on Downtown Los Angeles and gathering in front of Los Angeles City Hall. That same day, more than 1.4 million children from more than 123 countries protested in solidarity to demand the same thing: action.

Seven months on, Los Angeles has completed its third Global Climate Strike, and more will follow. Students from more than 150 countries worldwide struck on September 20, making this strike the largest climate demonstration in global, human history. Political candidates and major celebrities participated in Los Angeles and New York. Hopefully these influencers will take home the ideals of climate justice and continue to act on them long after September 20.

From the YCSLA team to all readers, we encourage you to attend the climate strikes hosted in your areas. We are not done striking. Several will continue regularly in California, with Los Angeles and the Bay Area being two of the largest ongoing strikes.

Go to StrikeWithUs.org for information on your local strikes. Be sure to send love and support to your local organizers, because they most likely need it.

If you’re a youth in California looking to set up a Youth Climate Strike of your own in your part of the state, reach out to Jesus from the Los Angeles team for support!

_—Jesus Villalba is a LAUSD student and an organizer of the Los Angeles Youth Climate_
