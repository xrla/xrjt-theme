---
title: People of Faith to be Pivotal in Climate Change
subtitle: Faith Groups Are Already Beginning To Lead, It Is "A Moral Responsibility"
author: Helen Peters
date: "00:00 10/07/2019"
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
  category: Rebellion Recorder
  author: Helen Peters
  tag:
    - Faith
    - Religion
hero_image: sunset.jpg
body_classes: overlay-light
# hero_caption: "CNN officials stop photography of an action by SoCal350, Climate Strike LA, and XRLA at their Los Angeles offices."
# header_image_credit: J. Matt
---

_Helen Peters — Journalist_

Americans don’t agree on much right now, but one thing is clear: this remains a majority religious country. Today, 75 percent of Americans claim faith of some kind (per Pew), and 50 percent of Americans belong to a house of worship (per Gallup).

Which means that in these last years remaining to avert apocalypse, communities of faith could prove pivotal. Fortunately, many faith groups have shown climate leadership at the highest levels: according to Arabella Advisors’ 2018 Global Divestment Report, religious institutions were responsible for 29 percent of fossil-fuel divestments in 2018 — more than any other type of organization.

The institutions’ public statements about these divestments tend to cite the moral imperative to act on the climate crisis and to safeguard human life and future generations. In the words of faithbased environmental group GreenFaith, "the world’s religious and spiritual traditions" share the belief that "protecting the Earth is a sacred act, and that environmental stewardship is a moral responsibility."

At the local level, faith communities’ efforts to tackle the climate crisis typically fall into three broad categories: greening rituals and houses of worship, community organizing, and working to educate congregations on the moral urgency of the crisis. The Global Muslim Climate Network, for instance, ultimately aims to call on Muslim nations to end their reliance on fossil fuels, but has begun with on-the-ground work at mosques, supporting the installation of solar panels and greening Ramadan ceremonies, as well as working on technology to facilitate green hajj. Nana Firman, cofounder of the Global Muslim Climate Network and Muslim Outreach Director for GreenFaith, told me that for many mosques, these practices serve as a springboard to further action.

Meanwhile, the Jewish Climate Action Network provides support and resources to help synagogues go solar, set climate goals, and establish green teams, among other actions. And the Catholic Climate Covenant is introducing environmental science and "care for creation" in thousands of Catholic schools, and has launched Catholic Energies, a project to help churches across the country switch to solar energy — starting with the largest solar array ever seen in D.C. Similar groups exist in most faith communities.

But despite the promising signs of divestment and the hard work of the growing body of religious climate activists, organizers of various faiths consulted by the Rebellion Recorder noted that, like the population at large, many congregations have yet to prioritize this most pressing challenge. Often, they say, they’re hard at work tackling other humanitarian crises of the day, such as the refugee crisis, poverty, and homelessness. In response, activists are working to highlight the ways the ways the climate emergency stokes and incites these other crises.

"Muslims around the world are facing a lot of other issues, right in front of their eyes, so they might not call climate change their priority," Nana Firman told me. "But you have conflict, you have refugee issues, you have Islamophobia, and it’s actually linked to climate change, in terms of resources, inequality," Firman notes. Similarly, Jackie Garcia Mann of the Interfaith Climate Action Network of Contra Costa County told me the message she brings to her community is: "Here we are trying to help a hundred thousand South and Central American refugees at our southern border, but there might be a hundred million refugees there 30 years from now."

Both Firman and Mann highlight that centralizing the climate crisis will depend on religious leaders at all levels picking up the cause. "You and me, we’re just activists," Firman told me. "People are not going to listen to us because we’re not a religious authority. But if they’re the ones who talk, from their pulpit, in their sermons, then people will understand." Mann agrees: "[Religious leaders] have a captive audience, and that audience does want to do good. They are looking for moral leadership, and this is the greatest moral issue of our time."

As inherently hierarchical institutions, religious groups offer the potential bonus of rapid mobilization of large groups of people, and the potential pitfall that the power to ignite such mobilization is in the hands of the few. In a recent op-ed entitled "A call for unified Jewish action on climate," Mann implored Jewish leaders and educators to speak the truth of the climate emergency to their congregations, loudly and repeatedly, until the community’s attention is unified on the crisis. Many in her interfaith network followed suit with similar op-eds.

Mann and her fellow organizers haven’t yet seen the kind of unified response they want from leaders. Until they do, they will fight on, knowing that it’s no longer enough to wait for leadership.

_—Helen Peters is a journalist and cultural critic who writes for a number of premier national and international news and cultural publications._
