---
title: Lost - Effects of Climate Migration
subtitle: Climate Migrations Have Already Begun And Will Worsen
author: Carmiel Banasky
date: "00:00 10/07/2019"
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
  category: Rebellion Recorder
  author: Carmiel Banasky
  tag: Migration
# hero_image: cnn-offices.jpg
# body_classes: overlay-light
# hero_caption: "CNN officials stop photography of an action by SoCal350, Climate Strike LA, and XRLA at their Los Angeles offices."
# header_image_credit: J. Matt
---

_Carmiel Banasky —Author, Educator_

"Lost" is the word those from Paradise, CA, use to describe their ongoing situation. The Camp Fire, the deadliest in California’s history, displaced 50,000 people. Around 20,000 migrated to Chico, 15 miles south.

Chico already had its share of problems before the fire, including a relentless housing shortage. Now, "for every one unit available, there’s demand for ten households," Ed Mayer, executive director of Housing Authority of the County of Butte, told NPR’s "After Paradise." Since this influx, Chico has experienced an increase in traffic accidents and crime, and a 16 percentage point increase in homelessness.

While many Chico residents want to help the newcomers, others, including some landlords, see the demand as a financial opportunity, raising rent prices on old and new tenants. Some long-time residents are fighting affordable housing units from coming into their neighborhoods.

Nine months after the fire, many survivors still live in tents. Families of five are sharing one motel bed. Others are in temporary FEMA housing. Deciphering FEMA protocol isn’t easy, and people who were already in need have a harder time qualifying for disaster aid. Those who cannot prove their residence in Paradise (because they weren’t officially on a lease, lost documentation, etc.) are sometimes barred from aid, as well as those who were unhoused prior to the fire. "We take care of our people," Ed Mayor told the Washington Post. Trump now plans to divert funds from FEMA’s disaster relief to pay for beds in detention centers on the border.

The story of Paradise and Chico exemplifies how climate migration exacerbates income inequality. The UN aptly calls this widening wealth gap "climate apartheid": the rich can buy their safety, while the poor lose even more. "Leaving itself sometimes imposes a significant cost — gas, missed work, hotel rooms — that the wealthier can bear but that the poor might not be able to," writes Annie Lowrey for The Atlantic.

In a UN report, Philip Alston states, "Perversely, while people in poverty are responsible for just a fraction of global emissions, they will bear the brunt of climate change, and have the least capacity to protect themselves." This pattern is most evident with indigenous subsistence villages in Alaska, who have arguably contributed the least carbon output, and are losing their homes to coastal erosion. The Denali Commission, which funds relocation efforts, is set to be eliminated by Trump.

Climate change is a "threat multiplier," compounding humanitarian crises — and both prompt migration. Rohingya refugees fled persecution (intensified by flooding and subsequent landslides) to Bangladesh, where, this July, a flood destroyed shelters in their settlement. In Guatemala, Honduras, and El Salvador, lack of farm work due to drought has forced many to choose between crossing into rival gang territory, or seeking asylum in the US.

The International Displacement Monitoring Center recorded that 17.2 million people were displaced due to disasters in 2018 alone. The first six months of 2019 have seen seven million displaced, "the highest midyear figure ever reported for displacements associated with disasters." And the center observes: "In today’s changing climate, mass displacement triggered by extreme weather events is becoming the norm." In our lifetimes, we may become displaced ourselves. There’s no blueprint for how take care of each other in the face of this loss.

But groups like the International Center for Climate Change and Development are trying to create "climate-resilient, migrant-friendly towns" that incentivize migration into less populated cities, and invest in industries that will generate employment. Perhaps this can be a model for the rest of the world.

_—Carmiel Banasky is a television writer, teacher, editor, activist, and the author of the novel "The Suicide of Claire Bishop."_
