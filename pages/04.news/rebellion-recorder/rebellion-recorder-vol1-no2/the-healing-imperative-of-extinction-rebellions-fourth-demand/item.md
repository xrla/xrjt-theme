---
title: The Healing Imperative of Extinction Rebellion's Fourth U.S. Demand
subtitle: XR Must Work To Center The Most Vulnerable Among Us
author: Alycee Lane
date: "00:00 10/07/2019"
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
  category: Rebellion Recorder
  author: Alycee Lane
  tag:
    - Regenerative Culture
    - Environmental Justice
hero_image: standla.jpg
body_classes: overlay-light-blue
hero_caption: "Demanding environmental justice with S.T.A.N.D. LA’S campaign to end neighborhood oil drilling in Los Angeles."
header_image_credit: J. Matt
---

_Alycee Lane —Author, Podcast Host_

In the opening salvo of its climate rebellion, XR US makes a simple, poignant demand: governments must tell the truth about climate change and our ecological emergency. Tell the truth.

For decades now, governments, primarily those of the global north, and especially the United States, have failed to tell the truth about the climate crisis. When forced to do so, most have evaded the necessary action by offering half-truths, at best. Some, including our own government, have even lied outright.

And so it makes profound sense that XR’s first demand is “tell the truth”. Telling the truth means taking responsibility, and acting. It means putting the interests of the people over profits. It means walking back from the edge of catastrophe.

Yet it would be a mistake to believe that XR lays truth-telling solely at the feet of our elected representatives. Actually, to think XR has only “governments” in mind is to misunderstand entirely the nature of its rebellion. For what XR also demands is that we — the rebels, the people — also tell the truth; and not merely the truth that we are facing an ecological emergency.

By including a demand to prioritize “the most vulnerable people and indigenous sovereignty,” as well as to establish “reparations and remediation led by and for Black people, Indigenous people, people of color and poor communities for years of environmental injustice,” XR insists that we tell the deeper truth about climate change. The truth, in the words of writer and philosopher James Trafford, is that this crisis is a “racist crisis.”

Indeed, climate change is ecological blowback from global systems of domination — colonialism, imperialism, and capitalism — that our energy systems powered (and continue to power), and with which our energy systems are hopelessly intertwined. Our energy systems serve the privileged and the powerful, at the expense of the most vulnerable. How could it be otherwise? After all, the most vulnerable have historically been excluded from decisions about energy policy, technology, and industry, including the siting of fossil-fuel plants — and indeed from decisions at every level of government and industry. Hence, Cancer Alley and Standing Rock, and the many other affected frontline communities whose rights and lives elected officials and corporations do not feel bound to respect.

By telling this deeper truth, we demystify the claim that “humans” caused climate change and reveal that what caused our crisis are systems from which some few people benefitted at the expense of many others. We must create the conditions to begin to repair the harms the most vulnerable have suffered, and are suffering now, as a consequence of the global north’s radical transformation of the world’s climate systems.

Yet, just as governments are uncomfortable telling the truth about our ecological crisis, many people are uncomfortable with speaking the deeper truth of a system structurally bound to disadvantage minority communities and its part in causing our climate crisis. Individuals are often as hesitant as governments to challenge the status quo. Some believe, for instance, that the systems of exploitation out of which climate change was produced are not relevant because they are a “political” matter rather than an “ecological” matter. Some worry that telling the truth will alienate potential allies. And some believe telling the truth unnecessarily expends the valuable energy and time we need to save the planet.  
 Of course, those who express these undoubtedly heartfelt beliefs and concerns do so in a context where extreme cyclones have ravaged places like Mozambique and Bangladesh; where catastrophic hurricanes have devastated Puerto Rico and other Caribbean Islands, leaving thousands dead in their wake; where super droughts have driven people from their homes in the Northern Triangle of Central America to the cages of our southern border; where rising seas are forcing island nations to search for new homes; where permafrost melt threatens the way of life of indigenous nations in the Northern Hemisphere.
Individuals reluctant to identify systemic bias and exploitation as foundational to the climate crisis voice their concerns about telling the deeper truth in a context where black and brown children who live in segregated communities are poisoned by the deadly pollution emitted from the refineries near their homes, their schools, and their playgrounds.

Yes, this deeper truth is uncomfortable. It is uncomfortable because it is about power and privilege, and their attendant harms. It demands a reckoning with our own everyday complicities — the small, mundane, and private acts and selfishly guarded privileges that produce disadvantage and which have thwarted the creation of a truly democratic society.

But we cannot rationally demand that our governments speak the truth about our ecological emergency and simultaneously demand that no one speak the truth that climate change is a racist crisis. Such is the beauty of XR US’s fourth demand, that it forces us to be true to the whole truth. And true we must be, for if ever there was a moment for truth telling, it’s now.

Thus, XR US exhorts us to:

1. Tell the truth — we are facing an ecological emergency.
1. Tell the truth that we have the technological capacity to reduce carbon emissions to net zero by 2025.
1. Tell the truth that only the oversight of the people will ensure government action.
1. Tell the truth — that in order to save ourselves, not only must we indict and then reject the systems of domination that produced this ecological emergency in the first place, but we must also acknowledge the harms they have caused, so that we might heal ourselves and ultimately heal our planet.

Without this demand our future becomes a repeat of our past, and we won’t survive that.

_— Mark Campbell teaches English in the LAUSD. ACE’s curriculums and lesson plans are available at ourclimateourfuture.org._
