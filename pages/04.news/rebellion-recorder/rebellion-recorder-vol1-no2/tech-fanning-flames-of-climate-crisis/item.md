---
title: Tech Fanning Flames Of Climate Crisis
subtitle: Allied With Big Oil, Military Industrial Complex, Big Tech Belies Its Green Public Relations Image
author: John Pfenning
date: "00:00 10/07/2019"
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
  category: Rebellion Recorder
  author: John Pfenning
  tag: Big Tech
hero_image: iceland-geothermal.jpg
body_classes: overlay-angry
hero_caption: "Iceland is prime server territory with tax breaks, built-in cooling, and geothermal power plants. Data centers use 2% of global elec- trical power today, projected to be 6% in ten years."
header_image_credit: J. Matt
---

_John Pfenning —Security Guard_

Big Tech has a big problem enabling the climate crisis. Huge data centers associated with these companies take massive amounts of energy to operate. The Guardian and The New York Times have reported that data centers worldwide generate as much carbon emissions as the global airline industry.

Less discussed are the ways that tech giants, namely Google, Facebook, Microsoft, and Amazon, actively support, collaborate, and do business with fossil fuel companies as well as the world’s largest polluter, the US military.

Last year Google started up an oil, gas, and energy division, and hired Darryl Willis, a 25-year vet of BP, as its vice president. The company has struck deals with French oil giant Total, Anadarko Petroleum, oilfield services company Baker & Hughes, and partnered with a Houston oil investment bank. This is all for the sake of making drilling for oil “smarter, cheaper, and more efficient,” according to tech and science journal Gizmodo.

These don’t represent the only alliances Google fails to trumpet in its feel-good public relations. Google has recently entered into talks with Saudi state-owned oil giant Aramco, even after the state murder of Washington Post journalist Jamal Khashoggi.

Despite the very public dropping of Project Maven, Google is still doing business with the US military. Through a venture capital subdivision At the urban public high school where I teach, the overwhelming majority of students — at least 90 percent — live at or below the poverty line, in immigrant families. They are currently receiving next to no education on climate catastrophe in their classrooms.

The global student climate strike is set for September 20, but my students will be missing from the front lines, not standing with their peers to fight for a future and a planet that are theirs, too. By failing to educate them, we are excluding them from social discourse and depriving them of social agency in the climate disaster.

My students are additionally excluded by the incessant fear of deportation and separation that the government inflicts on them and their families daily. And ironically, these young people are and will be key to the success of any effort to defeat the number one issue of our times, not only because of their sheer numbers, but also because of their vast reserves of talent, intelligence and courage — resources that are currently unused.

These students might receive a few lessons on the climate catastrophe in science classes, but there is no comprehensive curriculum to educate them on the greatest threat humanity has faced. I know the urgency and need for such a curriculum, but over the years I have found few to no resources, and have been called Gradient Ventures, Google provides funding and personnel for the company Cogniac, which develops AI that enhances drone warfare capabilities.

Facebook’s rap sheet is no less disconcerting. The social media giant has accepted over $5 million in advertising from ExxonMobil over the past year alone. In that same period, companies such as BP and Chevron have spent tens of thousands of dollars on Facebook advertising. According to quarterly sustainable energy journal The Beam, the American Petroleum Institute spent roughly $900,000 on Facebook ads from May 2018 to July 2019.

In the wake of the Cambridge Analytica scandal, Facebook also teamed up with the 58-year-old rightwing think tank The Atlantic Council. According to the reporting of MintPress News, the partnership is purportedly to “monitor alleged misinformation and foreign influence” in elections. To do so Facebook employs an organization whose principal business is lobbying on behalf of the U.S. – NATO military alliance. The Atlantic Council is sponsored by long-established defense contractors, Pentagon affiliates, wealthy Arab Gulf emirs, the British uncertain of how to effectively integrate climate change education and agency into my curriculum as an English teacher. Other teachers report similar difficulties.

Without the necessary guidance, time, and lesson plans, we have made little progress in making the climate a priority for our students.

Despite these obstacles, we teachers committed to the education of the underserved and underrepresented need to vow to develop and teach climate education in our classrooms. Thanks to organizations such as the Association for Climate Education (ACE), there is now a growing online teacher community committed to developing professional curriculums and offering lesson plans. These lessons are adaptable across grade levels and subject matter, and I am implementing them in three of my classes this fall.

As educators, it is our moral duty to provide our students with the skills that will enable them to reject the role of victim of the climate crisis, and instead become agents, battling on the picket lines of the future. Let this September 20 inspire our thousands and thousands of urban public schools and their teachers to step up, implement comprehensive climate education in their classrooms, and give their students a fair chance to fight for an earth worth living on.

Foreign Office, the U.S. Department of State, and the world’s largest fossil-fuel corporations, among other first-tier, wealthy corporate entities and multinationals.

The council’s stated mission is to encourage international diplomacy and promote nonviolent conflict resolution, but its actions and agenda point to a Western imperialist mission and the buttressing of current power and economic status quos. Since partnering with Facebook, dependent media outlets, especially those that are left-leaning and critical of war, have reportedly suffered major dips in their online traffic.

As reported in the Wall Street Journal, Amazon and Microsoft have also struck business agreements with Big Oil just last year — no Big Tech company is staying away as the world’s climate emergency rages. The fossil-fuel industry and military industrial complex are the greatest threats to life on Earth. By partnering with them, the tech giants whose services we engage with on a daily basis, who often boast about how they connect people, improve lives, and make the world a better place, are doing the exact opposite. It’s up to us to hold them accountable.
