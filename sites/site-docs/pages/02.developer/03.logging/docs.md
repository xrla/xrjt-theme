---
title: Error Logs
taxonomy:
    category: docs
---

PHP & NGINX logs our output via Docker logging & CMS logs are output to `/logs` directory in the absolute CMS root directory.  

# Grav logs
First thing is to get the Docker containers name so that you can interact with the Docker container. 